@extends('manage.layout')

@section('title', $title)

@section('content')
    @if(Session::has('message'))
            <div class="alert alert-success">
                <b>{!! Session::get("message") !!}</b>
            </div>
    @endif
      <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-success">Список</span> Показаны все платежи студента {{ $student_name }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>

    <div class="col-sm-6">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title mb-3">Добавить платеж</strong>
                    </div>
                    <div class="card-body">
                        <form action="{{ $action }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                            <input type="hidden" name="stud_id" value="{{ $stud_id }}">
                            Сумма поступления: <input type="text" name="sum" class="form-control">
                        <hr>
                        <input type="submit" value="Добавить платеж" class="btn btn-success">
                        </form>
                    </div>
                </div>
            </div>

    <div class="col-sm-6">
            <table id="bootstrap-data-table" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>Сумма оплаты</th>
                        <th>Дата поступления</th>
                        <th>Принял</th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($all_incomes as $income)
                            <tr>
                                <td>{{ $income->sum }}</td>
                                <td>{{ $income->created_at }}</td>
                                <td>{{ $income->who_took_name->name }} {{ $income->who_took_name->surname }}</td>
                            </tr>
                        @endforeach
                    </tbody>
            </table>
    </div>



@endsection

@section('datatable_js')
<script src="/manage_res/assets/js/lib/data-table/datatables.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/dataTables.bootstrap.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/dataTables.buttons.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.bootstrap.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/jszip.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/pdfmake.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/vfs_fonts.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.html5.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.print.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.colVis.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/datatables-init.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
          $('#bootstrap-data-table-export').DataTable();
        } );
    </script>

@endsection