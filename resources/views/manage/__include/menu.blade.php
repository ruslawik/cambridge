<ul class="nav navbar-nav">
                    <li class="active">
                        <a href="/manage/main"> <i class="menu-icon fa fa-dashboard"></i>Главная</a>
                    </li>

                    <h3 class="menu-title">Пользователи</h3><!-- /.menu-title -->
                        <li><a href="/manage/all_users"><i class="menu-icon fa fa-list"></i>Все</a></li>
                        <li><a href="/manage/add_user"><i class="menu-icon fa fa-plus"></i>Добавить</a></li>

                    <h3 class="menu-title">Студенты</h3><!-- /.menu-title -->
                        <li><a href="/manage/groups"><i class="menu-icon fa fa-users"></i>Группы</a></li>
                        <li><a href="/manage/results"><i class="menu-icon fa fa-list-alt"></i>Результаты</a></li>
                        <li><a href="/manage/income"><i class="menu-icon fa fa-dollar"></i>Платежи</a></li>

                    <h3 class="menu-title">Расписание</h3><!-- /.menu-title -->
                        <li><a href="/manage/calendar"><i class="menu-icon fa fa-tasks"></i>Установить</a></li>

                </ul>