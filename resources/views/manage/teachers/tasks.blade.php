@extends('manage.layout')

@section('title', $title)

@section('content')
    
    <style>

    p{
        color:black !important;
    }

    </style>

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul style="margin-left:20px;">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    
    <div class="col-sm-12">
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Задания</span> Отображаются все задания учителя <b>{{ $username }}. Пролистните вниз для добавления нового.</b><br> Вы также можете оставить комментарий к заданию.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            @if(Session::has('message'))
            <div class="alert alert-success">
                <b>{!! Session::get("message") !!}</b>
            </div>
            @endif


            <div class="col-sm-12" id="add_form" style="display: none;">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title mb-3">Добавить комментарий к заданию <span id="task_id_span"></span></strong> (<a href="#" onClick="show_add_comment_form(1,1);">Отмена</a>)
                    </div>
                    <div class="card-body">
                        <form action="{{ $action2 }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type='hidden' name="task_id" id="task_id" value="">
                        <textarea class="form-control" name="task_text" id="summary-ckeditor"></textarea>
                        
                        {!! Form::file('file1') !!}
                        {!! Form::file('file2') !!}
                        {!! Form::file('file3') !!}
                        <hr>
                        <input type="submit" value="Добавить" class="btn btn-success">
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title mb-3">Все задания</strong>
                    </div>
                    <div class="card-body">
                        <table id="bootstrap-data-table" class="table table-striped table-bordered">
                            <thead>
                               <tr>
                                  <td>№</td>
                                  <td>Текст</td>
                                  <td>Время отправки</td>
                                  <td>Приложенные файлы</td>
                               </tr>
                            </thead>
                        <tbody>
                <?php $k = 1; ?>
                @foreach ($all_tasks as $task)
                    <tr>
                        <td>{{ $k }}</td>
                        <td>
                            {!! $task->text !!}

                            <a onClick="show_add_comment_form({{ $task->id }}, {{ $k }});" href="#">Добавить комментарий</a> <i class="fa fa-comments-o"></i>

                            <hr>
                            @foreach ($task->comments as $comment)
                                <i style="font-size: 13px;">{{ $comment->user->name }} - {{ $comment->created_at }}</i>
                                {!! $comment->text !!}
                                @if ($comment->file1 != NULL )
                                    <a href="/manage/download/{{ $comment->id }}/1">Скачать файл 1 </a><br> 
                                @endif
                                @if ($comment->file2 != NULL )
                                    <a href="/manage/download/{{ $comment->id }}/2">Скачать файл 2 </a><br>
                                @endif
                                @if ($comment->file3 != NULL )
                                    <a href="/manage/download/{{ $comment->id }}/3">Скачать файл 3 </a><br>
                                @endif
                                <hr>
                            @endforeach
                        </td>
                        <td>{{ $task->created_at }}</td>
                        <td>
                            @if ($task->file1 != NULL )
                            <a href="/manage/download/{{ $task->id }}/1">Скачать файл 1 </a><br> 
                            @endif
                            @if ($task->file2 != NULL )
                            <a href="/manage/download/{{ $task->id }}/2">Скачать файл 2 </a><br>
                            @endif
                            @if ($task->file3 != NULL )
                            <a href="/manage/download/{{ $task->id }}/3">Скачать файл 3 </a><br>
                            @endif
                        </td>
                    </tr>
                    <?php $k++; ?>
                @endforeach
                </tbody>
            </table>
            </div>
            </div>
        </div>

            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title mb-3">Добавить НОВОЕ задание</strong>
                    </div>
                    <div class="card-body">
                        <form action="{{ $action }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type='hidden' name="user_id" value="{{ $teacher_id }}">
                        <textarea class="form-control" name="task_text" id="summary-ckeditor2"></textarea>
                        
                        {!! Form::file('file1') !!}
                        {!! Form::file('file2') !!}
                        {!! Form::file('file3') !!}
                        <hr>
                        <input type="submit" value="Задать" class="btn btn-success">
                        </form>
                    </div>
                </div>
            </div>

    </div>

@endsection

@section('editor_javascript')

<script>
    function show_add_comment_form(task_id, k){
        jQuery("#add_form").toggle('fast');
        jQuery('#task_id').val(task_id);
        jQuery('#task_id_span').html(k);
    }
</script>

<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<script>
    CKEDITOR.replace( 'summary-ckeditor' );
    CKEDITOR.replace( 'summary-ckeditor2' );
</script>
@endsection
