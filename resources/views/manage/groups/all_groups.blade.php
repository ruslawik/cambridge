@extends('manage.layout')

@section('title', $title)

@section('content')
    
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul style="margin-left:20px;">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    
    <div class="col-sm-12">
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Группы</span> Отображаются все группы. Нажмите на группу чтобы отредактировать
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            Название
            <form method="POST" action="{{ $action }}">
            	{{ csrf_field() }} 
            	<table class="table">
            		<tr>
            			<td>
            				<input type="text" name="group_name" class="form-control col-sm-12">
            			</td>
            			<td>
            				<input type="submit" value="Добавить" class="btn btn-success">
            			</td>
            		</tr>
            	</table>
            </form>
            <table class="table">
            	<tr>
            		<td>Название</td>
                    <td>Преподаватель</td>
            		<td>Дата добавления</td>
            	</tr>
            	@foreach ($all_groups as $group)
            		<tr>
            			<td><a href="/manage/edit_group/{{ $group->id }}">{{ $group->group_name }}</a></td>
                        <td>{{ $group->teacher()->exists()?$group->teacher->name." ".$group->teacher->surname:"Не назначен" }}</td>
            			<td>{{ $group->created_at }}</td>
            		</tr>
            	@endforeach
            </table>
    </div>

@endsection