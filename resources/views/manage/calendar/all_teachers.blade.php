@extends('manage.layout')

@section('title', $title)

@section('content')

    <div class="col-sm-12">
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Расписание</span> Отображаются Все преподаватели. Выберите кому Вы хотите установить расписание.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>

            <table id="bootstrap-data-table" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>Логин</th>
                        <th>Имя</th>
                        <th>Фамилия</th>
                        <th>Роль</th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($all_teachers as $user)
                            <tr>
                                <td><a href="/manage/teacher/{{ $user->id }}/set_calendar">{{ $user->login }}</a></td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->surname }}</td>
                                @if ($user->user_type == 1)
                                    <td>Администратор</td>
                                @endif
                                @if ($user->user_type == 2)
                                    <td>Клиент</td>
                                @endif
                                @if ($user->user_type == 3)
                                    <td>Учитель</td>
                                @endif
                            </tr>
                        @endforeach
                    </tbody>
            </table>
    </div>

@endsection