@extends('manage.layout')

@section('title', $title)

@section('content')

      <div class="col-sm-12">
                <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-warning">Внимание!</span> Вы можете добавлять новых пользователей и устанавливать их роли в системе.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-success">Описание ролей</span> <br><b>Администратор</b> - может редактировать тесты, добавлять новых пользователей, смотреть все результаты тестов<br><b>Студент</b> - имеет доступ к тестам, может проходить их, смотреть свои результаты
                  <br><b>Учитель</b> - может редактировать тесты, расписание, давать задания на дом
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>

    <div class="col-lg-12">
            <form method="POST" action="{{ $action }}">
                {{ csrf_field() }}
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Добавить пользователя</strong>
                        </div>
                        <div class="card-body">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div class="input-group">
                                            <div class="input-group-addon">Придумайте логин</div>
                                            <input type="text" class="form-control col-sm-5" name="login">
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Введите имя</div>
                                            <input type="text" class="form-control col-sm-5" name="name">
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Введите фамилию</div>
                                            <input type="text" class="form-control col-sm-5" name="surname">
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Введите номер телефона</div>
                                            <input type="text" class="form-control col-sm-5" name="tel_number">
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Придумайте пароль</div>
                                            <input type="text" class="form-control col-sm-5" name="password">
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Выберите роль</div>
                                            <select name="role" class="form-control col-sm-5">
                                                <option value="3">Учитель</option>
                                                <option value="2">Студент</option>
                                                <option value="1">Администратор</option>
                                            </select>
                            </div>
                            <button type="submit" class="btn btn-success" style="float:right;"><i class="fa fa-magic"></i>&nbsp; Добавить!</button>
                        </div>
                    </div>
            </form>
    </div>



@endsection