@extends('manage.layout')

@section('title', $title)

@section('content')

      <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-success">Ред.</span> Редактировать информацию пользователя {{ $username }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>

    <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">Изменить информацию</strong>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ $action }}">
                    {{ csrf_field() }}
                        <input type="hidden" name="user_id" value="{{ $user_id }}">
                        <div class="input-group">
                                <div class="input-group-addon">Фамилия</div>
                                <input type="text" class="form-control col-sm-5" name="surname" value="{{ $surname }}">
                        </div><br>
                        <div class="input-group">
                                <div class="input-group-addon">Имя</div>
                                <input type="text" class="form-control col-sm-5" name="name" value="{{ $name }}">
                        </div><br>
                        <div class="input-group">
                                <div class="input-group-addon">Номер телефона</div>
                                <input type="text" class="form-control col-sm-5" name="tel_number" value="{{ $tel_number }}">
                        </div><br>
                        <div class="input-group">
                                <div class="input-group-addon">Новый пароль (если нужно сменить)</div>
                                <input type="text" class="form-control col-sm-5" name="new_pass">
                        </div><br>
                        <input type="submit" value="Сохранить" class="btn btn-success">
                        <br>
                    </form>
                </div>
            </div>
    </div>



@endsection