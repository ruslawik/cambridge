<link rel="apple-touch-icon" href="/manage_res/apple-icon.png">
    <link rel="shortcut icon" href="/manage_res/favicon.ico">

    <link rel="stylesheet" href="/manage_res/assets/css/normalize.css">
    <link rel="stylesheet" href="/manage_res/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/manage_res/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="/manage_res/assets/css/themify-icons.css">
    <link rel="stylesheet" href="/manage_res/assets/css/flag-icon.min.css">
    <link rel="stylesheet" href="/manage_res/assets/css/cs-skin-elastic.css">
    <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.less"> -->
    <link rel="stylesheet" href="/manage_res/assets/scss/style.css">
    <link href="/manage_res/assets/css/lib/vector-map/jqvmap.min.css" rel="stylesheet">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>