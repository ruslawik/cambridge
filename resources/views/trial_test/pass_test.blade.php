@extends('trial_test.layout')

@section('title', $title)

@section('content')

    <div class="col-sm-12">
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">В процессе</span> Не забудьте нажать на кнопку "Завершить" по окончанию тестирования и <b>ОБЯЗАТЕЛЬНО ЗАПОЛНИТЕ ВСЕ ВОПРОСЫ!</b>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
    </div>
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul style="margin-left:20px;">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <div class="col-sm-12">
        <form action="{{ $action }}" method="POST">
            {{ csrf_field() }}
                <div class="card">
                    <div class="card-header">
                            <strong class="card-title">Ответьте на поставленные вопросы. Для завершения теста нажмите "Завершить"</strong>
                             <button type="submit" class="btn btn-success" style="float:right;"><i class="fa fa-magic"></i>&nbsp; Завершить!</button>
                        </div>
                    <div class="card-body">
                        <input type="hidden" value="{{ $questions->count() }}" name="total_questions" id="total_questions">
                        <input type="hidden" value="{{ $id }}" name="test_id" id="test_id">
                        <?php $k = 1; ?>
                        @foreach($questions as $question)
                        <div id="quest_{{$k}}" style="display: none;">
                            <i>Вопрос № {{ $k }} из {{ $questions->count() }}</i>
                            <br><hr>
                            <b>{{ $question->question }}</b>
                            <br>
                                <input type="hidden" name="question_{{ $k }}" value="{{ $question->id }}">
                                <table class="table">
                                @foreach ($question->answers as $answer)
                                    <tr><td><input type="radio" name="question_{{ $k }}_answer" value="{{ $answer->id }}"> </td> <td class="col-sm-3">{{ $answer->answer }}</td></tr>
                                @endforeach
                                </table>
                            <hr>
                            <a class="btn btn-warning" style="color:black;" onClick="show_quest({{ $k-1 }}, {{ $k }});">Предыдущий</a>
                            
                        <a class="btn btn-success" style="color:white;" onClick="show_quest({{ $k+1 }}, {{ $k }});">Следующий</a>

                        <a class="btn btn-warning" style="color:black;float:right;" onClick="show_quest(1, {{ $k }});">Вернуться в начало</a>
                        </div>
                            <?php $k++; ?>
                        @endforeach
                    </div>
                    
                </div>
        </form>
     </div>

@endsection

@section('javascript')
<script>
    function show_quest(next_id, now_id){
        if(next_id <= jQuery("#total_questions").val() && next_id >=1){
            jQuery("#quest_"+next_id).toggle('fast');
            jQuery("#quest_"+now_id).toggle('fast');
        }
    }
    show_quest(1,0);

</script>
@endsection