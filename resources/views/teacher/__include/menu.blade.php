<ul class="nav navbar-nav">
                    <li class="active">
                        <a href="/teacher/main"> <i class="menu-icon fa fa-dashboard"></i>Главная</a>
                    </li>

                    <h3 class="menu-title">Мои студенты</h3><!-- /.menu-title -->
                    <li><a href="/teacher/show_students"><i class="menu-icon fa fa-users"></i>Просмотреть</a></li>
                    <li><a href="/teacher/set_testing"><i class="menu-icon fa fa-tasks"></i>Назначить тестирование</a></li>
                    <li><a href="/teacher/give_hometask"><i class="menu-icon fa fa-tasks"></i>Задать задание</a></li>

                    <h3 class="menu-title">Мои тесты</h3><!-- /.menu-title -->
                        <li><a href="/teacher/edit_tests"><i class="menu-icon fa fa-pencil"></i>Редактировать</a></li>

                    <h3 class="menu-title">Мое расписание</h3><!-- /.menu-title -->
                        <li><a href="/teacher/show_calendar"><i class="menu-icon fa fa-eye"></i>Просмотреть</a></li>

                </ul>