@extends('teacher.layout')

@section('title', $title)

@section('content')

    <div class="col-sm-12">
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Назначить тестирование</span> Выберите студентов и время до которого студенты должны сдать тест
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            @if(Session::has('message'))
            <div class="alert alert-success">
                <b>{!! Session::get("message") !!}</b>
            </div>
            @endif
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title mb-3">Назначение тестирования</strong>
                    </div>
                    <div class="card-body">
                        <form action="{{ $action }}" method="POST">
                        {{ csrf_field() }}
                        <table class="table">
                        <tr>
                            <td>Выберите тест</td>
                            <td>Выберите крайний срок сдачи</td>
                        </tr>
                        <tr>
                            <td>
                            <select class="form-control" name="test_id">
                                    @foreach ($tests as $test)
                                        <option value="{{$test->id}}">{{ $test->name }}</option>
                                    @endforeach
                            </select>
                            </td>
                            <td>
                                 <input type="hidden" name="total_students" value="{{ $all_group_students->count() }}">
                                {{ Form::input('dateTime-local', 'test_deadline', null, ['class'=>'form-control']) }}
                            </td>
                        </tr>
                    </table>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title mb-3">Группа "{{ $group['0']->group_name }}"</strong>
                    </div>
                    <div class="card-body">
                        <table id="bootstrap-data-table" class="table table-striped table-bordered">
                            <thead>
                               <tr>
                                  <td>Имя</td>
                                  <td>Фамилия</td>
                                  <td><center>Выбрать</center></td>
                               </tr>
                            </thead>
                        <tbody>
                <?php $k = 1; ?>
                @foreach ($all_group_students as $student)
                    <tr>
                        <td>{{ $student->user->name }}</td>
                        <td>{{ $student->user->surname }}</td>
                        <td><center><input type="checkbox" name="st{{ $k }}" value="{{ $student->user->id }}" checked></center></td>
                    </tr>
                    <?php $k++; ?>
                @endforeach
                </tbody>
            </table>
                <input type="submit" value="Назначить" class="btn btn-success">
            </form>
            </div>
            </div>
        </div>

    </div>

@endsection