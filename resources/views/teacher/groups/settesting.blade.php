@extends('teacher.layout')

@section('title', $title)

@section('content')

    <div class="col-sm-12">
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Назначить тестирование</span> Выберите тест и группу для тестирования
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title mb-3">Все группы</strong>
                    </div>
                    <div class="card-body">
                        <table id="bootstrap-data-table" class="table table-striped table-bordered">
                            <thead>
                               <tr>
                                  <td>Название</td>                                
                                  <td><center>Назначить</center></td>
                               </tr>
                            </thead>
                        <tbody>
                @foreach ($groups as $group)
                    <tr>
                        <td>{{ $group->group_name }}</td>
                        <td><center><a href="/teacher/set_test_to_group/{{ $group->id }}"><i class="fa fa-plus" aria-hidden="true" style="color:green;"></i></a></center></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            </div>
            </div>
        </div>

    </div>

@endsection