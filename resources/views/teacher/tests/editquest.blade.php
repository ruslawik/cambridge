@extends('teacher.layout')

@section('title', $title)

@section('content')
    <div class="col-sm-12">
       
    </div>
    <div class="col-sm-6">
         <a href="/teacher/testedit/{{ $test['0']->id }}" class="btn btn-success" style="color:white;">Назад к вопросам</a>
         <a href="/teacher/testedit/delete_q/{{$question['0']->id}}/{{ $test['0']->id }}" class="btn btn-danger" style="color:white;float:right;">Удалить вопрос</a>
            <hr>
            <form action="{{ $action }}" method="POST">
            	<input type="hidden" name="test_id" value="{{ $test_id }}">
                <input type="hidden" name="q_id" value="{{$question['0']->id}}">
                <input type="hidden" name="ans_num" value="{{$question['0']->answers->count()}}">

                {{ csrf_field() }}
                Введите вопрос:
                <textarea name="question" class="form-control" rows=5>{{$question['0']->question}}</textarea>
                <br>
                <input type="submit" value="Сохранить" class="form-control btn btn-success col-sm-3">
            
    </div>
    <div class="col-sm-6">
        <a href="/teacher/testedit/add_answer_to_q/{{$question['0']->id}}" class="btn btn-success" style="color:white;">Добавить ответ</a>
        <hr>
        Ответы вопроса:
        <?php $k = 1; ?>
        @foreach ($question['0']->answers as $answer)
            <table><tr><td><input style='width:30px;' type='checkbox' name='quest_{{$question['0']->id}}_ans_{{$k}}_right' class='form-control' {{ $answer->is_right?"checked":"" }}></td><td>

                            <input type='hidden' class="form-control" value='{{$answer->id}}' name='quest_{{$question['0']->id}}_ans_{{$k}}_bdid'>

                            <input type='text' class="form-control" value='{{$answer->answer}}' id='quest_{{$question['0']->id}}_ans_{{$k}}' name='quest_{{$question['0']->id}}_ans_{{$k}}'>
                            </td><td>&nbsp;<a href="/teacher/testedit/delete_answer/{{$answer->id}}"><i class='fa fa-trash'></i></a>
                            </td></tr></table>
            <?php $k++; ?> 
        @endforeach
        </form>
    </div>

@endsection