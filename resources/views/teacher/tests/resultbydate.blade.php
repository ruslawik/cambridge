@extends('teacher.layout')

@section('title', $title)

@section('content')

    <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                            <strong class="card-title">Пользователь - {{ $name }} {{ $surname }} || Дата сдачи - {{ $date }}</strong>
                        </div>
                    <div class="card-body">
                        <h4>Тест "{{ $test['0']->name }}"</h4>
                        <hr>
                        <?php $right = 0; ?>
                        @foreach ($all_questions as $question)
                            <b>{{ $question->question->question }}</b>
                            <br>
                            @if($question->answer->answer === $question->question->right_answer($question->question_id)['0']->answer)
                                <font color="green"><b>Ваш ответ: <i>{{ $question->answer->answer }}</i></b></font><br>
                                <?php $right++; ?>
                            @else
                                <font color="red"><b>Ваш ответ: <i>{{ $question->answer->answer }}</i></b></font><br>
                                <font color="green"><b>Верный ответ: <i>{{ $question->question->right_answer($question->question_id)['0']->answer }}
                                </i></b></font>
                            @endif
                            </i>
                            <hr>
                        @endforeach
                        <font color="blue"><b>Общее количество вопросов: {{ $all_questions->count() }}</b></font><br>
                        <font color="green"><b>Верных ответов: {{ $right }}</b></font><br>
                        <font color="red"><b>Неверных ответов: {{ $all_questions->count()-$right }}</b></font>
                    </div>
                </div>
    </div>

@endsection