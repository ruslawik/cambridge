@extends('student.layout')

@section('title', $title)

@section('content')
	<style>
		p{
			color:black !important;
		}
	</style>
    <div class="col-sm-12">
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Приветствуем!</span> Добро пожаловать, {{ $username }}!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
    </div>

    <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title mb-3">Лента событий</strong>
                    </div>
                    <div class="card-body">
                        @foreach ($hometasks as $hometask)
                        	<b><i>Задание от {{ $hometask->created_at }}</i></b><br>
                        	{!! $hometask->hometask_text !!}
                        	Приложенные файлы<br>
                        	@if ($hometask->file1 != NULL )
                        	<a href="/student/download/{{ $hometask->id }}/1">Скачать файл 1 </a><br> 
                        	@endif
                        	@if ($hometask->file2 != NULL )
                        	<a href="/student/download/{{ $hometask->id }}/2">Скачать файл 2 </a><br>
                        	@endif
                        	@if ($hometask->file3 != NULL )
                        	<a href="/student/download/{{ $hometask->id }}/3">Скачать файл 3 </a><br>
                        	@endif <hr>
                        @endforeach
                        @if($hometasks->count() === 0)
                            Пока ничего нет...
                        @endif
            		</div>
       			</div>
    </div>

@endsection