@extends('student.layout')

@section('title', $title)

@section('content')

    @if(Session::has('message'))
            <div class="alert alert-success">
                <b>{!! Session::get("message") !!}</b>
            </div>
    @endif

    <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                            <strong class="card-title">Пройденные тесты</strong>
                        </div>
                    <div class="card-body">
                     @foreach ($results as $key => $result)
                        Завершенное тестирование <b> {{ $tests[$result['0']['test_id']]['0']->name}} </b>- {{ $key }}
                        <a href="/student/test_result/{{ $result['0']['test_id'] }}/{{ $key }}" class="btn-sm btn-success" style="color:white;cursor:pointer;"> Просмотреть</a>
                        <br><br>
                    @endforeach
                </div>
    </div>

@endsection