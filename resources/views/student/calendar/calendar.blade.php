@extends('student.layout')

@section('title', $title)

@section('content')

    <div class="col-sm-12">
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Расписание</span> Удачного дня!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
    </div>

    <div class="col-md-4">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title mb-3">Понедельник</strong>
                            </div>
                            <div class="card-body">
                                <div class="mx-auto d-block">
                                    @foreach($list as $element)
                                        @if($element->apta_kuni === 1)
                                            {{$element->hours<10?"0":""}}{{ $element->hours }}:{{$element->minutes<10?"0":""}}{{ $element->minutes }} - {{ $element->predmet_name }}<br>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
            </div>
            <div class="col-md-4">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title mb-3">Вторник</strong>
                            </div>
                            <div class="card-body">
                                <div class="mx-auto d-block">
                                    @foreach($list as $element)
                                        @if($element->apta_kuni === 2)
                                            {{$element->hours<10?"0":""}}{{ $element->hours }}:{{$element->minutes<10?"0":""}}{{ $element->minutes }} - {{ $element->predmet_name }}<br>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
            </div>

            <div class="col-md-4">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title mb-3">Среда</strong>
                            </div>
                            <div class="card-body">
                                <div class="mx-auto d-block">
                                    @foreach($list as $element)
                                        @if($element->apta_kuni === 3)
                                            {{$element->hours<10?"0":""}}{{ $element->hours }}:{{$element->minutes<10?"0":""}}{{ $element->minutes }} - {{ $element->predmet_name }}<br>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
            </div>
             <div class="col-md-4">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title mb-3">Четверг</strong>
                            </div>
                            <div class="card-body">
                                <div class="mx-auto d-block">
                                    @foreach($list as $element)
                                        @if($element->apta_kuni === 4)
                                            {{$element->hours<10?"0":""}}{{ $element->hours }}:{{$element->minutes<10?"0":""}}{{ $element->minutes }} - {{ $element->predmet_name }}<br>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
            </div>
        <div class="col-md-4">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title mb-3">Пятница</strong>
                            </div>
                            <div class="card-body">
                                <div class="mx-auto d-block">
                                    @foreach($list as $element)
                                        @if($element->apta_kuni === 5)
                                            {{$element->hours<10?"0":""}}{{ $element->hours }}:{{$element->minutes<10?"0":""}}{{ $element->minutes }} - {{ $element->predmet_name }}<br>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
            </div>
            <div class="col-md-4">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title mb-3">Суббота</strong>
                            </div>
                            <div class="card-body">
                                <div class="mx-auto d-block">
                                    @foreach($list as $element)
                                        @if($element->apta_kuni === 6)
                                            {{$element->hours<10?"0":""}}{{ $element->hours }}:{{$element->minutes<10?"0":""}}{{ $element->minutes }} - {{ $element->predmet_name }}<br>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
            </div>

            <div class="col-md-4">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title mb-3">Воскресенье</strong>
                            </div>
                            <div class="card-body">
                                <div class="mx-auto d-block">
                                    @foreach($list as $element)
                                        @if($element->apta_kuni === 7)
                                            {{$element->hours<10?"0":""}}{{ $element->hours }}:{{$element->minutes<10?"0":""}}{{ $element->minutes }} - {{ $element->predmet_name }}<br>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
            </div>

@endsection