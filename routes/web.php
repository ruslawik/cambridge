<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
	Route::get('', 'LoginController@getLogin');
	Route::get('login', 'LoginController@getLogin');
	Route::post('login', 'LoginController@postLogin');
	Route::any('logout', 'LoginController@getLogout');
	Route::any('local/{lang}', 'LocalizationController@setLocale');
	Route::any('trial_test', 'TrialTestController@getIndex');

	//Защита админки посредником через роль аккаунта из БД (админ - 1, юзер - 2)
	Route::group(['middleware' => ['auth.admin'], 'prefix' => 'manage'], function () {
		Route::any('main', 'Manage\ManageController@getIndex');
		Route::any('all_users', 'Manage\ManageController@allUsers');
		Route::any('add_user', 'Manage\ManageController@getAddUser');
		Route::any('save_user', 'Manage\ManageController@savePostNewUser');

		Route::any('edit_tests', 'Manage\ManageController@getEditTests');		
		Route::any('save_new_test', 'TestController@saveNewTest');	
		Route::get('testedit/{id}', 'TestController@getTestEdit');	
		Route::post('save_edited_test', 'TestController@saveEditedTest');

		Route::any('calendar', "Manage\ManageController@calendarAll");
		Route::any('teacher/{id}/set_calendar', "Manage\ManageController@setCalendar");
		Route::post('save_new_calendar', "Manage\ManageController@setCalendarPost");
		Route::get('calendar/delete/{id}', "Manage\ManageController@deleteCalendar");

		Route::any('groups', 'Manage\ManageController@getGroups');	
		Route::post('save_new_group', 'Manage\ManageController@saveNewGroup');	
		Route::any('edit_group/{id}', 'Manage\ManageController@getEditGroup');
		Route::any('add_student/{id}/to_group/{gr_id}', 'Manage\ManageController@add_stud_to_group');	
		Route::any('delete_student/{id}/from_group/{gr_id}', 'Manage\ManageController@del_stud_from_group');
		Route::any('put_new_teacher', 'Manage\ManageController@putNewTeacherToGroup');

		Route::any('teacher/{teacher_id}/set_task', 'Manage\ManageController@setTask');
		Route::post('set_task_post', "Manage\ManageController@setTaskPOST");
		Route::post('addTaskComment', "Manage\ManageController@addTaskComment");

		Route::any('results', "Manage\ManageController@results");

		Route::any('student/{stud_id}', 'Manage\ManageController@studentInfo');
		Route::any('download_hometask/{id}/{file_num}', 'Manage\ManageController@download_hometask');
		Route::any('download/{id}/{file_num}', 'Manage\ManageController@download');

		Route::any('test_result/{stud_id}/{test_id}/{date}', 'Manage\ManageController@testResultByDate');

		Route::any('edit_user/{user_id}', 'Manage\ManageController@editUser');
		Route::any('save_edited_user', 'Manage\ManageController@editUserSave');

		Route::any('income', 'Manage\ManageController@studentsIncome');
		Route::any('student_income/{stud_id}', 'Manage\ManageController@studentIncome');
		Route::post('add_income', "Manage\ManageController@addIncomePOST");

	});

	//Для учителя через посредник, проверяющий на роль учителя и авторизацию
	Route::group(['middleware' => ['auth.teacher'], 'prefix' => 'teacher'], function () {
		Route::any('main', 'Teacher\TeacherController@getIndex');

		Route::any('edit_tests', 'Teacher\TeacherController@getEditTests');
		Route::any('save_new_test', 'Teacher\TeacherController@saveNewTest');
		Route::get('testedit/{id}', 'Teacher\TeacherController@getTestEdit');
		Route::get('testedit/add_new_q_to/{test_id}/{q_num}', 'Teacher\TeacherController@addNewQuest');
		Route::post('testedit/addnewquestpost', 'Teacher\TeacherController@addNewQuestPOST');
		Route::get('testedit/question_edit/{id}/{test_id}', 'Teacher\TeacherController@editQuest');
		Route::post('testedit/question_edit/save', 'Teacher\TeacherController@editQuestPOST');
		Route::get('testedit/add_answer_to_q/{q_id}', 'Teacher\TeacherController@addAnswer');
		Route::get('testedit/delete_answer/{a_id}', 'Teacher\TeacherController@delAnswer');
		Route::post('testedit/save_time_to', 'Teacher\TeacherController@saveTimeToSolve');
		Route::get('testedit/delete_q/{q_id}/{test_id}', 'Teacher\TeacherController@delQuest');
		Route::get('testedit/delete_test/{test_id}', 'Teacher\TeacherController@delTest');

		Route::any('show_calendar', 'Teacher\TeacherController@showCalendar');

		Route::any('show_students', 'Teacher\TeacherController@showStudents');

		Route::any('set_testing', 'Teacher\TeacherController@showSetTesting');
		Route::any('set_test_to_group/{gr_id}', 'Teacher\TeacherController@setTestToGroup');
		Route::post('set_test_post', 'Teacher\TeacherController@setTestingPOST');

		Route::any('give_hometask', 'Teacher\TeacherController@giveHometask');
		Route::any('give_homework_to_group/{gr_id}', 'Teacher\TeacherController@giveHomeworkToGroup');
		Route::post('giveHometaskPOST', 'Teacher\TeacherController@giveHometaskPOST');

		Route::any('student_info/{stud_id}', 'Teacher\TeacherController@studentInfo');

		Route::any('download/{id}/{file_num}', 'Teacher\TeacherController@download');
		Route::any('download_task/{id}/{file_num}', 'Teacher\TeacherController@download_task');

		Route::any('test_result/{stud_id}/{test_id}/{date}', 'Teacher\TeacherController@testResultByDate');

		Route::post('add_comment', 'Teacher\TeacherController@addComment');

	});

	//Для обычного юзера роуты через простой посредник, проверяющий только авторизацию
	Route::group(['middleware' => ['auth.user'], 'prefix' => 'student'], function () {
		Route::any('main', 'Student\StudentController@getIndex');

		Route::any('pass_tests', 'Student\StudentController@getTests');
		Route::any('pass_test/{id}', 'Student\StudentController@passTest');

		Route::post('save_test', 'Student\StudentController@saveTest');

		Route::any('results_tests', 'Student\StudentController@allResults');

		Route::any('test_result/{test_id}/{date}', 'Student\StudentController@testResultByDate');

		Route::any('calendar', 'Student\StudentController@showCalendar');

		Route::any('download/{id}/{file_num}', 'Student\StudentController@download');

		Route::any('hometask', 'Student\StudentController@getHometask');

	});