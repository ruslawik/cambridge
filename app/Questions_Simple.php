<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Answer;

class Questions_Simple extends Model
{
	protected $table = "questions_simple";
    
	protected $fillable = array('question', 'local_id', 'test_id');

	public function answers()
    {
        return $this->hasMany('App\Answer', 'question_id', 'id');
    }

    public function right_answer ($q_id){
    	return Answer::where("question_id", $q_id)->where('is_right', 1)->get();
    }

}
