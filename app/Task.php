<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Task extends Model
{
    protected $table = "tasks";

    public function comments (){
    	return $this->hasMany('App\Task', 'parent_id', 'id');
    }

    public function user (){
    	return $this->hasOne('App\User', 'id', 'author_id');
    }
}
