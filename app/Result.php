<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Questions_Simple;
use App\Answer;

class Result extends Model
{
    protected $table = "results";
    	
   	public function question (){
   		return $this->hasOne("App\Questions_Simple", 'id', 'question_id');
   	}

   	public function answer (){
   		return $this->hasOne("App\Answer", 'id', 'answer_id');	
   	}

}