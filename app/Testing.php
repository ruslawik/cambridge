<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Test;

class Testing extends Model
{
    protected $table = "testings";

    public function test (){
    	return $this->hasOne('App\Test', 'id', 'test_id');
    }

}
