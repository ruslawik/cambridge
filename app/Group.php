<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User; use App\Flow;

class Group extends Model
{
    protected $table = "groups";

    public function teacher(){

    	return $this->hasOne("App\User", 'id', 'teacher_id');

    }

    public function students (){
    	return $this->hasMany('App\Flow', 'group_id', 'id');
    }
    
}
