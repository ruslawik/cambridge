<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Auth;

class UserAuth {
    protected $auth;

    function __construct(Guard $auth) {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next){
        if ($this->auth->guest())
            return redirect()->action('LoginController@getLogin')->with('error', 'Введите логин и пароль для доступа');
        if(Auth::user()->user_type!=2)
        	return redirect()->action('LoginController@getLogin')->with('error', 'Вы не можете иметь доступ к панели студента');
        return $next($request);
    }
}
