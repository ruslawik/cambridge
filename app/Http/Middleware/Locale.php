<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Auth;

class Locale {
    protected $auth;

    function __construct(Guard $auth) {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next){
        if(isset(Auth::user()->locale)){
            $locale = Auth::user()->locale;
        }else{
            $locale = "en";
        }
        app()->setLocale($locale);
        
        return $next($request);
    }
}
