<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Test;
use App\Questions_Simple;

class TrialTestController extends Controller
{
    public function getIndex (){
    	$id = 6;
    	$ar['title'] = "Пройти тест на определение уровня";

    	$test = Test::where('id', $id)->get();

    	$ar['test'] = $test;
    	$ar['questions'] = Questions_Simple::where('test_id', $id)->get();
    	$ar['action'] = action("Student\StudentController@saveTest");
    	$ar['id'] = $id;

    	return view('trial_test.pass_test', $ar);
    }
}
