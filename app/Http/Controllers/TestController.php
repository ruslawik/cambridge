<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Test;
use App\Questions_Simple;
use App\Answer;
use Validator;

class TestController extends Controller
{

    public function saveNewTest (Request $r){

        $messages = [
            'required' => 'Необходимо заполнить поле',
        ];

        $validator = Validator::make($r->all(), [
                'new_test_name' => 'required',
            ], $messages);

            if ($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput($r->input());
            }


    	$test = new Test();
        $test->name = $r->input('new_test_name');
        $test->user_id = Auth::id();
        $test->save();

    	return back();
    }

    public function getTestEdit ($id){

        $ar = array();
        $ar['title'] = 'Редактировать тест';
        $ar['action'] = action('TestController@saveEditedTest');
        $ar['test'] = Test::where("id", $id)->get();
        $ar['q_num'] = Questions_Simple::where("test_id", $id)->max('local_id');
        $ar['questions'] = Questions_Simple::where("test_id", $id)->get();

        return view("manage.tests.edittest", $ar);
    }

    public function saveEditedTest (){

        $kol = $_POST['quest_num'];
        $ins_id = $_POST['ins_id'];

        for($i=1; $i<=$kol; $i++){
            //Если вопрос вообще существует
            if(isset($_POST['quest_'.$i])){
                //Если вопрос уже был в базе (обновляем), значит должен быть quest_bdid_
                if(isset($_POST['quest_bdid_'.$i])){
                    $q_bdid = $_POST['quest_bdid_'.$i];
                    $q_text = $_POST['quest_'.$i];
                    //Обновить вопрос
                    Questions_Simple::find($q_bdid)->update(['question' => $q_text]);
                    //Обновляем ответы вопроса
                    $ans_num = $_POST['quest_'.$i.'_ans_num'];
                    for($j=0; $j<=$ans_num; $j++){
                        //Если ответ  передан клиентом, который есть в базе
                        if(isset($_POST['quest_'.$q_bdid.'_ans_'.$j])){
                            //Если ответ есть в базе, обновляем
                            if(isset($_POST["ans_bdid_".$q_bdid."_".$j]) && $_POST["ans_del_bdid_".$q_bdid."_".$j]!="DEL"){
                                $ans_text = $_POST['quest_'.$q_bdid.'_ans_'.$j];
                                if(isset($_POST['quest_'.$q_bdid.'_ans_'.$j.'_right'])){
                                    $q_right = $_POST['quest_'.$q_bdid.'_ans_'.$j.'_right'];
                                    $right = 1;
                                }else{
                                    $right = 0;
                                }
                                $ansid = $_POST["ans_bdid_".$q_bdid."_".$j];
                                Answer::where("id", $ansid)->where('question_id', $q_bdid)->update(['answer'=>$ans_text, 'is_right' => $right]);
                            }
                        }else{
                            //Если ответ не передан и был в базе, удаляем
                            if(isset($_POST["ans_del_bdid_".$q_bdid."_".$j]) && $_POST["ans_del_bdid_".$q_bdid."_".$j]=="DEL"){
                                $ansid = $_POST["ans_bdid_".$q_bdid."_".$j];
                                Answer::find($ansid)->delete();
                            }
                        }
                        //Если ответ передан, но не был в базе
                        if(isset($_POST['quest_'.$i.'_ans_'.$j]) && !isset($_POST['ans_bdid_'.$q_bdid.'_'.$j])){
                                //Если ответ новый, добавляем
                                $ans_text = $_POST['quest_'.$i.'_ans_'.$j];

                                if(isset($_POST['quest_'.$q_bdid.'_ans_'.$j.'_right'])){
                                    $q_right = $_POST['quest_'.$q_bdid.'_ans_'.$j.'_right'];
                                    $right = 1;
                                }else{
                                    $right = 0;
                                }

                                $answer = new Answer();
                                $answer->question_id = $q_bdid;
                                $answer->answer = $ans_text;
                                $answer->is_right = $right;
                                $answer->save();
                        }
                    }
                }else{
                    //Если вопрос новый (добавляем)
                    $q_text = $_POST['quest_'.$i];

                    $new_question = new Questions_Simple();
                    $new_question->question = $q_text;
                    $new_question->local_id = $i;
                    $new_question->test_id = $ins_id;
                    $new_question->save();

                    $quest_id = $new_question->id;
                    //Добавляем ответы вопроса
                    $ans_num = $_POST['quest_'.$i.'_ans_num'];
                    for($y=0; $y<=$ans_num; $y++){
                        if(isset($_POST['quest_'.$i.'_ans_'.$y])){
                            $ans_text = $_POST['quest_'.$i.'_ans_'.$y];
                            
                            if(isset($_POST['quest_'.$i.'_ans_'.$y.'_right'])){
                                    $q_right = $_POST['quest_'.$i.'_ans_'.$y.'_right'];
                                    $right = 1;
                            }else{
                                    $right = 0;
                            }

                            $new_answer = new Answer();
                            $new_answer->answer = $ans_text;
                            $new_answer->is_right = $right;
                            $new_answer->question_id = $quest_id;
                            $new_answer->save();
                        }
                    }
                }
            }else{
                //Если вопрос не существует (удален клиентом, но был в базе)
                if(isset($_POST['quest_del_bdid_'.$i]) && $_POST['quest_del_bdid_'.$i]=="DEL" ){
                    $q_bdid = $_POST['quest_bdid_'.$i];
                    Questions_Simple::find($q_bdid)->delete();
                    Answer::where('question_id', $q_bdid)->delete();
                }
            }
        }


        return back();
    }
}
