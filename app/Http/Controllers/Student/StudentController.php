<?php

namespace App\Http\Controllers\Student;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Test; use App\Questions_Simple; use App\Answer; use App\Testing; use App\Result; use App\Flow;
use App\Calendar; use App\Hometask; use Carbon\Carbon; use App\Now_Passing;
use Auth;
use Session;
use Validator;


class StudentController extends Controller
{
 	public function getIndex (){
        $ar = array();
        $ar['title'] = "CambridgeStudy";
        $ar['username'] = Auth::user()->name;

        $ar['hometasks'] = Hometask::where('student_id', Auth::id())->orderBy('id', 'DESC')->get();

        return view('student.main', $ar);
    }   

    public function getTests (){
    	$ar = array();
        $ar['title'] = "Все тесты";
        $ar['username'] = Auth::user()->name;
        $ar['all_testings'] = Testing::where('student_id', Auth::id())->orderBy('id','DESC')->get();
        $ar['date'] = Carbon::now('Asia/Almaty');

    	return view('student.test.testings', $ar);
    }

    public function passTest ($id){
        $ar = array(); 
        $test = Test::where('id', $id)->get();

        $now_passing = Now_Passing::where('student_id', Auth::id())
                                    ->where('test_id', $id)->get();
        if($now_passing->count()>=1){
            $now_time = Carbon::now();
            $diff_in_minutes = $now_time->diffInMinutes(Carbon::parse($now_passing['0']->created_at));
            if($test['0']->time_to_solve > $diff_in_minutes){
               $ar['is_available'] = 1;
               $ar['time_left'] = $test['0']->time_to_solve - $diff_in_minutes;
            }else{
               $ar['is_available'] = 0;
               $ar['time_left'] = 0;
            }
        }else{
            $now_passing = new Now_Passing();
            $now_passing->student_id = Auth::id();
            $now_passing->test_id = $id;
            $now_passing->save();
            $ar['is_available'] = 1;
            $ar['time_left'] = $test['0']->time_to_solve;
        }

    	$ar['title'] = 'Тестирование';
    	$ar['test'] = $test;
    	$ar['questions'] = Questions_Simple::where('test_id', $id)->get();
    	$ar['action'] = action("Student\StudentController@saveTest");
    	$ar['id'] = $id;
    
    	return view('student.test.pass_test', $ar);	
    }

    public function saveTest (Request $r){

    	$total_questions = $r->input('total_questions');
    	$answer_time = Carbon::now('Asia/Almaty');
    	$to_ins = array();
    	$test_id = $r->input('test_id');

    	$messages = [
            'required' => 'На все вопросы необходимо ответить',
        ];

    	for($i=1; $i<=$total_questions; $i++){

    		$validator = Validator::make($r->all(), [
                'question_'.$i => 'required',
                'question_'.$i."_answer" => 'required',
            ], $messages);

            if ($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput($r->input());
            }

    		$quest_id = $r->input('question_'.$i);
    		$answer_id = $r->input('question_'.$i."_answer");
    		
    		$to_ins[] = array("test_id" => $test_id, "question_id"=>$quest_id, "answer_id" => $answer_id, "user_id" => Auth::user()->id, "created_at" => $answer_time);
    	}

    	Result::insert($to_ins);
    	Session::flash('message', "Тестирование успешно завершено! Теперь Вы можете просмотреть Ваши результаты");
    	return redirect('/student/results_tests');
    }

    public function allResults (){

    	$ar = array();
        $ar['title'] = "Все пройденные тесты";
        $ar['username'] = Auth::user()->name;

        $ar['date'] = Carbon::now('Asia/Almaty');

        $results = Result::where("user_id", Auth::user()->id)
        							->orderBy('id', 'DESC')
                                    ->get()
                                    ->groupBy(function($pool) {
                                                return $pool->created_at->toDateTimeString();
                                             })
                                    ->toArray();
         $tests = array();
        foreach ($results as $key => $value) {
        	$tests[$value['0']['test_id']] = Test::where("id", $value['0']['test_id'])->get();
        }
        $ar['tests'] = $tests;
        $ar['results'] = $results;

    	return view('student.test.all_results', $ar);
    }

    public function testResultByDate ($test_id, $date){
    	$ar = array();
        $ar['title'] = "Детальные результаты";
        $ar['name'] = Auth::user()->name;
        $ar['surname'] = Auth::user()->surname;
        $ar['date'] = $date;
        $ar['test'] = Test::where('id', $test_id)->get();

        $ar['all_questions'] = Result::where('user_id', Auth::id())
        								->where('created_at', $date)
        								->get();

    	return view('student.test.resultbydate', $ar);
    }

    public function showCalendar (){
        $ar = array();
        $ar['title'] = 'Просмотреть расписание';
        $teacher_ids = array();

        $flows = Flow::where('student_id', Auth::id())->get();
        foreach ($flows as $key => $flow) {
            $teacher_ids[] = $flow->flow_teacher['0']->teacher_id;
        }

        $ar['list'] = Calendar::whereIn("teacher_id", $teacher_ids)->orderBy('hours', 'asc')->orderBy('minutes', 'asc')->get();

        return view("student.calendar.calendar", $ar);
    }

    public function download ($id, $file_num){

        $id = Hometask::where('id', $id)->pluck('file'.$file_num);
        return response()->download(public_path($id['0']));
    }

    public function getHometask (){
        $ar = array();
        $ar['title'] = "Домашнее задание";
        $ar['username'] = Auth::user()->name;

        $ar['hometasks'] = Hometask::where('student_id', Auth::id())->orderBy('id', 'DESC')->get();

        return view('student.hometask.hometask_show', $ar);
    }

}
