<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Application;
use Hash;
use Auth;

class LoginController extends Controller{
    function getLogin (){
        
        /*
        $user = new User();
        $user->login = 'admin';
        $user->password = Hash::make('346488');
        $user->name = "Ruslan";
        $user->surname = "Khuzin";
        $user->save();
        */
        $ar = array();
        $ar['action'] = action('LoginController@postLogin');
        
        return view('manage.login', $ar);
    }

    function postLogin(Request $request){
        if (!Auth::attempt(['login' => $request->input('login'), 'password' => $request->input('password')]))
            return back()->with('error', 'Неверный логин/пароль');

        if (Auth::user()->user_type == 1)
            return redirect()->action('Manage\ManageController@getIndex');
        else if (Auth::user()->user_type == 2)
            return redirect()->action('Student\StudentController@getIndex');
        else if (Auth::user()->user_type == 3)
            return redirect()->action('Teacher\TeacherController@getIndex');
        else abort(404);
    }

    function getLogout(){
        Auth::logout();

        return redirect()->to('/login');
    }
}
