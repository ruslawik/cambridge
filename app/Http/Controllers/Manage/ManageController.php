<?php
namespace App\Http\Controllers\Manage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Application;
use App\Test; use App\Questions_Simple; use App\Answer; use App\Calendar; use App\Group; use App\Flow;
use Hash;
use App\Task;
use App\Hometask;
use App\Testing;
use Carbon\Carbon;
use App\Result;
use App\Income;
use Auth;
use Session;
use Validator;

class ManageController extends Controller {

    function getIndex (){
        $ar = array();
        $ar['title'] = "CambridgeStudy";
        $ar['username'] = Auth::user()->name;
        $ar['all_teachers'] = User::where("user_type", "3")->get();

        return view('manage.main', $ar);
    }

    function allUsers (){
    	$ar = array();
        $ar['title'] = "Все пользователи";
        $ar['users'] = User::all();

        return view('manage.users.all_users', $ar);
    }

    function getAddUser (){
        $ar = array();
        $ar['title'] = 'Добавить нового пользователя';
        $ar['action'] = action('Manage\ManageController@savePostNewUser');

        return view("manage.users.add_user", $ar);

    }

    function savePostNewUser (Request $request){
        $validatedData = $request->validate([
            'login' => 'required|unique:users|max:255',
            'name' => 'required',
            'surname' => 'required',
            'password' => 'required'
        ]);

        $new_user = new User;
        $new_user->login = $request->input('login');
        $new_user->name = $request->input('name');
        $new_user->surname = $request->input('surname');
        $new_user->tel_number = $request->input('tel_number');
        $new_user->user_type = $request->input('role');
        $new_user->password = Hash::make($request->input('password'));
        $new_user->save();
        
        return back();
    }

    public function getEditTests (){
    	$ar = array();
        $ar['title'] = 'Редактировать тесты';
        $ar['action'] = action('TestController@saveNewTest');
        $ar['all_tests'] = Test::all();



        return view("manage.tests.all_tests", $ar);

    }

    public function calendarAll (){
        $ar = array();
        $ar['title'] = 'Редактировать расписание';
        $ar['all_teachers'] = User::where("user_type", "3")->get();

        return view("manage.calendar.all_teachers", $ar);
    }

    public function setCalendar ($id){
        $ar = array();
        $ar['title'] = 'Редактировать расписание';
        $ar['teacher'] = User::where("user_type", "3")->where("id", $id)->get();
        $ar['action'] = action('Manage\ManageController@setCalendarPost');
        $ar['list'] = Calendar::where("teacher_id", $id)->orderBy('hours', 'asc')->orderBy('minutes', 'asc')->get();

        return view("manage.calendar.set_calendar", $ar);
    }

    public function setCalendarPost (Request $r){
        $calendar = new Calendar();

        $messages = [
            'required' => 'Необходимо заполнить поле',
        ];

        $validator = Validator::make($r->all(), [
                'predmet_name' => 'required',
            ], $messages);

            if ($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput($r->input());
            }

        $calendar->teacher_id = $r->input('for_teacher');
        $calendar->hours = $r->input('hours');
        $calendar->minutes = $r->input('minutes');
        $calendar->predmet_name = $r->input('predmet_name');
        $calendar->apta_kuni = $r->input('dni');

        $calendar->save();

        return back();
    }

    public function deleteCalendar ($id){

        Calendar::where("id", $id)->delete();
        return back();
    }

    public function getGroups (){

        $ar = array();
        $ar['title'] = 'Редактировать группы';
        $ar['action'] = action('Manage\ManageController@saveNewGroup');
        $ar['all_groups'] = Group::all();

        return view("manage.groups.all_groups", $ar);

    }

    public function saveNewGroup (Request $r){

         $messages = [
            'required' => 'Необходимо заполнить поле',
        ];

        $validator = Validator::make($r->all(), [
                'group_name' => 'required',
            ], $messages);

            if ($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput($r->input());
            }

        $group = new Group();
        $group->group_name = $r->input('group_name');
        $group->save();

        return back();
    }

    public function getEditGroup ($id){
        $ar = array();
        $ar['title'] = 'Редактировать группу';
        $ar['all_students'] = User::where('user_type', 2)->get();
        $ar['group'] = Group::where('id', $id)->get();
        $ar['all_group_students'] = Flow::where('group_id', $id)->get();
        $ar['teachers'] = User::where('user_type', 3)->get();
        $ar['action'] = action('Manage\ManageController@putNewTeacherToGroup');

        return view("manage.groups.edit_group", $ar);
    }

    public function add_stud_to_group ($id, $gr_id){

        $add_stud_to_group = new Flow();
        $add_stud_to_group->student_id = $id;
        $add_stud_to_group->group_id = $gr_id;
        $add_stud_to_group->save();

        return back();
    }

    public function del_stud_from_group ($id, $gr_id){

        Flow::where('student_id', $id)->where('group_id', $gr_id)->delete();

        return back();
    }

    public function putNewTeacherToGroup (Request $r){

        $group_id = $r->input('group_id_hidden');
        $new_teacher_id = $r->input('new_teacher_id');

        Group::where('id', $group_id)->update(["teacher_id" => $new_teacher_id]);

        return back();
    }

     public function download ($id, $file_num){

        $id = Task::where('id', $id)->pluck('file'.$file_num);
        return response()->download(public_path($id['0']));
    }


    public function setTask ($teacher_id){
        $ar = array();
        $ar['title'] = "Задачи";
        $teacher = User::where('id', $teacher_id)->get();
        $ar['username'] = $teacher['0']->name." ".$teacher['0']->surname;
        $ar['action'] = action("Manage\ManageController@setTaskPOST");
        $ar['action2'] = action("Manage\ManageController@addTaskComment");
        $ar['all_tasks'] = Task::where("user_id", $teacher_id)->orderBy('id', 'DESC')->get();
        $ar['teacher_id'] = $teacher['0']->id;

        return view('manage.teachers.tasks', $ar);   
    }

    public function setTaskPOST (Request $r){

        $messages = [
            'required' => 'Необходимо заполнить поле',
        ];

        $validator = Validator::make($r->all(), [
                'task_text' => 'required',
            ], $messages);

            if ($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput($r->input());
            }
        
         $destinationPath = "task_files/".Auth::user()->name."_".Auth::id();

        if($r->has('file1')){
                $file1 = $r->file('file1');
                $file_name1 = rand(1,9999).$file1->getClientOriginalName();
                $file1->move($destinationPath, $file_name1);
        }
        if($r->has('file2')){
                $file2 = $r->file('file2');
                $file_name2 = rand(1,9999).$file2->getClientOriginalName();
                $file2->move($destinationPath, $file_name2);
        }
        if($r->has('file3')){
                $file3 = $r->file('file3');
                $file_name3 = rand(1,9999).$file3->getClientOriginalName();
                $file3->move($destinationPath, $file_name3);
        }

        $task = new Task();

        $task->text = $r->input('task_text');
        $task->user_id = $r->input('user_id');
        $task->author_id = Auth::id();
        $r->has('file1') ? $task->file1 = $destinationPath."/".$file_name1 : "";
        $r->has('file2') ? $task->file2 = $destinationPath."/".$file_name2 : "";
        $r->has('file3') ? $task->file3 = $destinationPath."/".$file_name3 : "";
        $task->save();

        Session::flash('message', "Задание успешно отправлено!");
        return back();
    }

    public function addTaskComment (Request $r){

        $destinationPath = "task_files/".Auth::user()->name."_".Auth::id();

        if($r->has('file1')){
                $file1 = $r->file('file1');
                $file_name1 = rand(1,9999).$file1->getClientOriginalName();
                $file1->move($destinationPath, $file_name1);
        }
        if($r->has('file2')){
                $file2 = $r->file('file2');
                $file_name2 = rand(1,9999).$file2->getClientOriginalName();
                $file2->move($destinationPath, $file_name2);
        }
        if($r->has('file3')){
                $file3 = $r->file('file3');
                $file_name3 = rand(1,9999).$file3->getClientOriginalName();
                $file3->move($destinationPath, $file_name3);
        }

        $task = new Task();

        $task->text = $r->input('task_text');
        $task->user_id = 0;
        $task->parent_id = $r->input('task_id');
        $task->author_id = Auth::id();
        $r->has('file1') ? $task->file1 = $destinationPath."/".$file_name1 : "";
        $r->has('file2') ? $task->file2 = $destinationPath."/".$file_name2 : "";
        $r->has('file3') ? $task->file3 = $destinationPath."/".$file_name3 : "";
        $task->save();

        Session::flash('message', "Комментарий успешно отправлено!");
        return back();
    }

    public function results (){
        $ar = array();
        $ar['title'] = "Результаты всех студентов";
        $ar['all_students'] = User::where('user_type', '2')->get();

        return view('manage.students.results', $ar);   
    }

    public function studentInfo ($stud_id){
        $ar = array();
        $ar['title'] = 'Информация о студенте';
        $ar['student'] = User::where('id', $stud_id)->get();
        $ar['hometasks'] = Hometask::where('student_id', $stud_id)->orderBy('id', 'DESC')->get();
        $ar['testings'] = Testing::where('student_id', $stud_id)->orderBy('id','DESC')->get();

        $ar['date'] = Carbon::now('Asia/Almaty');

        $results = Result::where("user_id", $stud_id)
                                    ->orderBy('id', 'DESC')
                                    ->get()
                                    ->groupBy(function($pool) {
                                                return $pool->created_at->toDateTimeString();
                                             })
                                    ->toArray();
        $tests = array();
        foreach ($results as $key => $value) {
            $tests[$value['0']['test_id']] = Test::where("id", $value['0']['test_id'])->get();
        }
        $ar['tests'] = $tests;
        $ar['results'] = $results;


        return view("manage.students.studentinfo", $ar);
    }

    public function download_hometask ($id, $file_num){

        $id = Hometask::where('id', $id)->pluck('file'.$file_num);
        return response()->download(public_path($id['0']));
    }

    public function testResultByDate ($stud_id, $test_id, $date){
        $ar = array();
        $ar['title'] = "Детальные результаты";
        $student = User::where('id', $stud_id)->get();
        $ar['name'] = $student['0']->name;
        $ar['surname'] = $student['0']->surname;
        $ar['date'] = $date;
        $ar['test'] = Test::where('id', $test_id)->get();

        $ar['all_questions'] = Result::where('user_id', $stud_id)
                                        ->where('created_at', $date)
                                        ->get();

        return view('manage.students.resultbydate', $ar);
    }

    public function editUser ($user_id){
        $ar = array();
        $ar['title'] = "Редактор";
        $user = User::where('id', $user_id)->get();
        $ar['username'] = $user['0']->name." ".$user['0']->surname;
        $ar['action'] = action('Manage\ManageController@editUserSave');
        $ar['surname'] = $user['0']->surname;
        $ar['name'] = $user['0']->name;
        $ar['user_id'] = $user['0']->id;
        $ar['tel_number'] = $user['0']->tel_number;

        return view('manage.users.edit', $ar);
    }

    public function editUserSave (Request $r){

        User::where('id', $r->input('user_id'))->update(['name'=>$r->input('name'), 'surname'=>$r->input('surname'), 'tel_number'=>$r->input('tel_number')]);

        if($r->has('new_pass') && $r->input('new_pass')!="" && $r->input('new_pass')!=" "){
            User::where('id', $r->input('user_id'))->update(['password'=>Hash::make($r->input('new_pass'))]);
        }

        return back();
    }

    public function studentsIncome (){
        $ar = array();
        $ar['title'] = "Платежи студентов";
        $ar['all_students'] = User::where('user_type', '2')->get();

        return view('manage.students.income_student_list', $ar);
    }

    public function studentIncome ($stud_id){

        $ar = array();
        $ar['title'] = "Платежи";
        $ar['all_incomes'] = Income::where('student_id', $stud_id)->get();
        $student = User::where('id', $stud_id)->get();
        $ar['student_name'] = $student['0']->name." ".$student['0']->surname; 
        $ar['action'] = action("Manage\ManageController@addIncomePOST");
        $ar['stud_id'] = $stud_id;

        return view('manage.students.student_personal_income', $ar);
    }

    public function addIncomePOST (Request $r){

        $sum = $r->input('sum');

        if($sum == "" || $sum == " " || $sum == "   "){
            $sum = 0;
        }

        $newInc = new Income();
        $newInc->sum = $sum;
        $newInc->student_id = $r->input('stud_id');
        $newInc->who_took_id = Auth::id();
        $newInc->save();

        Session::flash('message', "Платеж успешно добавлен в базу!");
        return back();
    }

}
