<?php

namespace App\Http\Controllers\Teacher;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Test; use App\Questions_Simple; use App\Answer; use App\Calendar; use App\Group; use App\Flow;
use App\Testing; use App\Hometask; use App\User; use App\Result; use App\Task; use App\Now_Passing;
use Session;
use Auth;
use Carbon\Carbon;
use Validator;

class TeacherController extends Controller
{
    function getIndex (){
        $ar = array();
        $ar['title'] = "CambridgeStudy";
        $ar['username'] = Auth::user()->name;

        $ar['all_tasks'] = Task::where("user_id", Auth::id())->orderBy('id', 'DESC')->get();
        $ar['action'] = action('Teacher\TeacherController@addComment');

        return view('teacher.main', $ar);
    }
    public function getEditTests (){
    	$ar = array();
        $ar['title'] = 'Редактировать тесты';
        $ar['action'] = action('Teacher\TeacherController@saveNewTest');
        $ar['all_tests'] = Test::where("user_id", Auth::id())->get();

        return view("teacher.tests.all_tests", $ar);
    }

    public function getTestEdit ($id){

        $ar = array();
        $ar['title'] = 'Редактировать тест';
        $ar['test'] = Test::where("id", $id)->where("user_id", Auth::id())->get();
        $q_num = Questions_Simple::where("test_id", $id)->max('local_id');
        if($q_num <= 0){
            $q_num = 0;
        }
        $ar['q_num'] = $q_num;
        $ar['questions'] = Questions_Simple::where("test_id", $id)->get();
        $ar['action'] = action('Teacher\TeacherController@saveTimeToSolve');

        return view("teacher.tests.edittest", $ar);
    }

    public function saveTimeToSolve (Request $r){

        $test_id = $r->input('ins_id');
        $time_to_solve = $r->input('time_to_solve');

        Test::where('id', $test_id)->update(['time_to_solve'=>$time_to_solve]);

        return back();
    }
    
    public function showCalendar (){
        $ar = array();
        $ar['title'] = 'Просмотреть расписание';
        $ar['list'] = Calendar::where("teacher_id", Auth::id())->orderBy('hours', 'asc')->orderBy('minutes', 'asc')->get();

        return view("teacher.calendar.calendar", $ar);
    }

    public function saveNewTest (Request $r){
        $messages = [
            'required' => 'Необходимо заполнить поле',
        ];

        $validator = Validator::make($r->all(), [
                'new_test_name' => 'required',
            ], $messages);

            if ($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput($r->input());
            }

        $test = new Test();
        $test->name = $r->input('new_test_name');
        $test->user_id = Auth::id();
        $test->save();

        return back();
    }

    public function delTest ($test_id){
        Test::where('id', $test_id)->delete();
        $qs = Questions_Simple::where('test_id', $test_id)->get();
        Questions_Simple::where('test_id', $test_id)->delete();
        foreach ($qs as $q) {
            Answer::where('question_id', $q->id)->delete();
        }
        return back();
    }

    public function addNewQuest ($test_id, $q_num){

        $ar = array();
        $ar['title'] = 'Добавить вопрос';
        $ar['action'] = action('Teacher\TeacherController@addNewQuestPOST');
        $ar['test'] = Test::where("id", $test_id)->where("user_id", Auth::id())->get();
        $ar['test_id'] = $test_id;
        $ar['q_num'] = $q_num;

        return view("teacher.tests.addnewquest", $ar);
    }

    public function addNewQuestPOST (Request $r){

        $new_quest = $r->input('new_question');
        $test_id = $r->input('test_id');
        $q_num = $r->input('q_num');
        $q_num++;
        $new = new Questions_Simple();
        $new->question = $new_quest;
        $new->local_id = $q_num;
        $new->test_id = $test_id;
        $new->save();

        return redirect('/teacher/testedit/question_edit/'.$new->id."/".$test_id);
    }

    public function editQuest ($id, $test_id){
        $ar = array();
        $ar['action'] = action('Teacher\TeacherController@editQuestPOST');
        $ar['test'] = Test::where("id", $test_id)->where("user_id", Auth::id())->get();
        $ar['test_id'] = $test_id;
        $ar['question'] = Questions_Simple::where('id', $id)->get();
        $ar['title'] = 'Ред. вопрос '.$ar['question']['0']->local_id.' в тесте "'.$ar['test']['0']->name.'"';


        return view("teacher.tests.editquest", $ar);
    }

    public function delQuest ($q_id, $test_id){

        Questions_Simple::where('id', $q_id)->delete();
        Answer::where('question_id', $q_id)->delete();

        return redirect("/teacher/testedit/".$test_id);
    }

    public function editQuestPOST (Request $r){

        $q_id = $r->input('q_id');
        $ans_num = $r->input('ans_num');
        $question = $r->input('question');
        Questions_Simple::where('id', $q_id)->update(["question"=>$question]);

        for($i=1; $i<=$ans_num; $i++){
            if($r->has('quest_'.$q_id.'_ans_'.$i.'_right')){
                $is_right = 1;
            }else{
                $is_right = 0;
            }
            $answer = $r->input('quest_'.$q_id.'_ans_'.$i);
            $bdid = $r->input('quest_'.$q_id.'_ans_'.$i.'_bdid');
            Answer::where('id', $bdid)->update(["answer"=>$answer, "is_right"=>$is_right]);
        }

        return back();
    }

    public function addAnswer ($q_id){

        $new = new Answer();
        $new->question_id = $q_id;
        $new->save();

        return back();
    }

    public function delAnswer ($a_id){

        Answer::where('id', $a_id)->delete();

        return back();
    }




    public function showStudents (){

        $ar = array();
        $ar['title'] = 'Ваши группы';
        $ar['groups'] = Group::where('teacher_id', Auth::id())->get();

        return view("teacher.groups.show", $ar);

    }

    public function showSetTesting (){

        $ar = array();
        $ar['title'] = 'Назначить тестирование';
        $ar['groups'] = Group::where('teacher_id', Auth::id())->get();
        $ar['tests'] = Test::where('user_id', Auth::id())->get();

        return view("teacher.groups.settesting", $ar);
    }

    public function setTestToGroup (Request $r, $gr_id){
        $ar = array();
        $ar['title'] = 'Назначить тестирование';
        $ar['tests'] = Test::where('user_id', Auth::id())->get();
        $ar['group'] = Group::where('id', $gr_id)->get();
        $ar['all_group_students'] = Flow::where('group_id', $gr_id)->get();
        $ar['action'] = action("Teacher\TeacherController@setTestingPOST");


        return view("teacher.groups.test_to_group", $ar);

    }

    public function setTestingPOST (Request $r){

        $test_id = $r->input('test_id');
        $test_deadline = $r->input('test_deadline');
        $total_students = $r->input('total_students');

        $k = 0;
        for($i=1; $i<=$total_students; $i++){
            if( $r->has("st".$i) ){
                $k++;
                $new_testing_for_student = new Testing();
                $new_testing_for_student->student_id = $r->input("st".$i);
                $new_testing_for_student->test_id = $test_id;
                $new_testing_for_student->deadline = $test_deadline;
                $new_testing_for_student->save();
                Now_Passing::where('student_id', $r->input("st".$i))->where('test_id', $test_id)->delete();
            }
        }
        if($k!=0)
                Session::flash('message', "Тестирование успешно назначено!");
            else
                Session::flash('message', "Не выбраны студенты для назначения тестирования");
        return back();
    }

    public function giveHometask (){
        $ar = array();
        $ar['title'] = "Задать домашнее задание";
        $ar['groups'] = Group::where('teacher_id', Auth::id())->get();


        return view('teacher.hometask.select_for_task', $ar);
    }

    public function giveHomeworkToGroup ($gr_id){

        $ar = array();
        $ar['title'] = 'Задать домашнее задание';
        $ar['group'] = Group::where('id', $gr_id)->get();
        $ar['all_group_students'] = Flow::where('group_id', $gr_id)->get();
        $ar['action'] = action("Teacher\TeacherController@giveHometaskPOST");

        return view("teacher.hometask.task_to_group", $ar);
    }

    public function giveHometaskPOST (Request $r){

        $messages = [
            'required' => 'Необходимо заполнить поле',
        ];

        $validator = Validator::make($r->all(), [
                'hometask_text' => 'required',
            ], $messages);

            if ($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput($r->input());
            }
        
        $destinationPath = "hometask_files/".Auth::user()->name."_".Auth::id();

        if($r->has('file1')){
                $file1 = $r->file('file1');
                $file_name1 = rand(1,9999).$file1->getClientOriginalName();
                $file1->move($destinationPath, $file_name1);
        }
        if($r->has('file2')){
                $file2 = $r->file('file2');
                $file_name2 = rand(1,9999).$file2->getClientOriginalName();
                $file2->move($destinationPath, $file_name2);
        }
        if($r->has('file3')){
                $file3 = $r->file('file3');
                $file_name3 = rand(1,9999).$file3->getClientOriginalName();
                $file3->move($destinationPath, $file_name3);
        }

        for($i=1; $i<=$r->input('total_students'); $i++){
            if($r->has('st'.$i)){
                $hometask = new Hometask();

                $hometask->hometask_text = $r->input('hometask_text');
                $hometask->student_id = $r->input('st'.$i);
                $r->has('file1') ? $hometask->file1 = $destinationPath."/".$file_name1 : "";
                $r->has('file2') ? $hometask->file2 = $destinationPath."/".$file_name2 : "";
                $r->has('file3') ? $hometask->file3 = $destinationPath."/".$file_name3 : "";
                $hometask->save();
            }
        }
        Session::flash('message', "Домашнее задание успешно отправлено!");
        return back();
    }

    public function download ($id, $file_num){

        $id = Hometask::where('id', $id)->pluck('file'.$file_num);
        return response()->download(public_path($id['0']));
    }

    public function download_task ($id, $file_num){

        $id = Task::where('id', $id)->pluck('file'.$file_num);
        return response()->download(public_path($id['0']));
    }


    public function studentInfo ($stud_id){
        $ar = array();
        $ar['title'] = 'Информация о студенте';
        $ar['student'] = User::where('id', $stud_id)->get();
        $ar['hometasks'] = Hometask::where('student_id', $stud_id)->orderBy('id', 'DESC')->get();
        $ar['testings'] = Testing::where('student_id', $stud_id)->orderBy('id','DESC')->get();

        $ar['date'] = Carbon::now('Asia/Almaty');

        $results = Result::where("user_id", $stud_id)
                                    ->orderBy('id', 'DESC')
                                    ->get()
                                    ->groupBy(function($pool) {
                                                return $pool->created_at->toDateTimeString();
                                             })
                                    ->toArray();
        $tests = array();
        foreach ($results as $key => $value) {
            $tests[$value['0']['test_id']] = Test::where("id", $value['0']['test_id'])->get();
        }
        $ar['tests'] = $tests;
        $ar['results'] = $results;


        return view("teacher.groups.studentinfo", $ar);
    }

    public function testResultByDate ($stud_id, $test_id, $date){
        $ar = array();
        $ar['title'] = "Детальные результаты";
        $student = User::where('id', $stud_id)->get();
        $ar['name'] = $student['0']->name;
        $ar['surname'] = $student['0']->surname;
        $ar['date'] = $date;
        $ar['test'] = Test::where('id', $test_id)->get();

        $ar['all_questions'] = Result::where('user_id', $stud_id)
                                        ->where('created_at', $date)
                                        ->get();

        return view('teacher.tests.resultbydate', $ar);
    }

    public function addComment (Request $r){

        $messages = [
            'required' => 'Необходимо заполнить поле',
        ];

        $validator = Validator::make($r->all(), [
                'task_text' => 'required',
            ], $messages);

            if ($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput($r->input());
            }

        $destinationPath = "task_files/".Auth::user()->name."_".Auth::id();

        if($r->has('file1')){
                $file1 = $r->file('file1');
                $file_name1 = rand(1,9999).$file1->getClientOriginalName();
                $file1->move($destinationPath, $file_name1);
        }
        if($r->has('file2')){
                $file2 = $r->file('file2');
                $file_name2 = rand(1,9999).$file2->getClientOriginalName();
                $file2->move($destinationPath, $file_name2);
        }
        if($r->has('file3')){
                $file3 = $r->file('file3');
                $file_name3 = rand(1,9999).$file3->getClientOriginalName();
                $file3->move($destinationPath, $file_name3);
        }

        $task = new Task();

        $task->text = $r->input('task_text');
        $task->user_id = 0;
        $task->parent_id = $r->input('task_id');
        $task->author_id = Auth::id();
        $r->has('file1') ? $task->file1 = $destinationPath."/".$file_name1 : "";
        $r->has('file2') ? $task->file2 = $destinationPath."/".$file_name2 : "";
        $r->has('file3') ? $task->file3 = $destinationPath."/".$file_name3 : "";
        $task->save();

        Session::flash('message', "Комментарий успешно отправлен!");
        return back();
    }

}
