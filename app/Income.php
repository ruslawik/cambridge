<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Income extends Model
{
    protected $table = "incomes";

    public function who_took_name (){
    	return $this->hasOne('App\User', 'id', 'who_took_id');
    }
    
}
