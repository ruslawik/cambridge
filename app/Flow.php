<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Group;

class Flow extends Model
{
    protected $table = "flows";

    public function user (){
    	
    	return $this->hasOne('App\User', 'id', 'student_id');
    }

    public function flow_teacher (){
    	return $this->hasMany('App\Group', 'id', 'group_id');
    }
    
}
